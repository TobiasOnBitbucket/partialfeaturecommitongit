REQUIRES:
-------------
JAVA 13.0.2
The bash-script "git-pfc" needs to be called from the project directory

INSTALLATION:
-------------
Copy bash-file "git-pfc" and project jar "JGitPFC.jar" into your Git script folder, e.g. C:\User\Name\bin
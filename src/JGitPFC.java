/*************************************************************
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
*************************************************************/
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.ResetCommand;
import org.eclipse.jgit.api.errors.GitAPIException;
import FAXE.EmbeddedAnnotation;
import FAXE.FAXE;
import org.apache.commons.cli.*;

public class JGitPFC {
	
	/**
	 * Extracts unique feature names from @EmbeddedAnnoations list
	 * @param eaList input list
	 * @return List<String> with unique feature names 
	 */
	private static List<String> extractUniqueFeatureNamesfromList(List<EmbeddedAnnotation> eaList) {
		HashSet<String> hSet = new HashSet<String>();
		for(EmbeddedAnnotation ea : eaList) {
			hSet.add(ea.getFeature());
		}
		
		List<String> newList = new ArrayList<String>(hSet);
		java.util.Collections.sort(newList);
		return newList;
	}
	
	public static void main(String[] args) {
		System.out.println(">>> JGitPFC");

		/*****************************
		/** (0) INTERPET ARGUMENTS **/
		/****************************/
		// From https://stackoverflow.com/questions/367706/how-do-i-parse-command-line-arguments-in-java
        Options options = new Options();

        Option cmdFeature = new Option("f", "feature", true, "Single feature to consider");
        cmdFeature.setRequired(true);
        options.addOption(cmdFeature);

        Option cmdWorkingDirectory = new Option("wd", "working-directory", true, "Path to git-folder (with .git) of project. (data provided by calling bash script)");
        cmdWorkingDirectory.setRequired(true);
        options.addOption(cmdWorkingDirectory);
        
        Option cmdMessage = new Option("m", "message", true, "Text for commit message");
        cmdMessage.setRequired(false);
        options.addOption(cmdMessage);

        Option cmdNoCommit = new Option("nc", "no-commit", false, "Suppress git-commit");
        cmdNoCommit.setRequired(false);
        options.addOption(cmdNoCommit);
        
        Option cmdPrintAnnoation = new Option("p", "print-embedded-annotations", false, "Prints available embedded annotations");
        cmdPrintAnnoation.setRequired(false);
        options.addOption(cmdPrintAnnoation);
        
        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd = null;

        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            formatter.printHelp("utility-name", options);

            System.exit(1);
        }

        String featureName = cmd.getOptionValue("feature");
        String commitMessage = cmd.getOptionValue("message");
        String workingDirectory = cmd.getOptionValue("working-directory");
        boolean commitNoCommit = false;
        if(cmd.hasOption("no-commit")) {
        	commitNoCommit = true;
        }
        if(cmd.hasOption("print-embedded-annotations")) {
        	// Print embedded annotations and end program
        	System.out.println("Print embedded annotations and end program");
        	List<EmbeddedAnnotation> listEA = FAXE.extractEAfromRootDirectory(workingDirectory);
        	if(featureName==null || featureName.isBlank()) {
        		System.out.println("Available embedded annotations in " +workingDirectory);
        		System.out.println(extractUniqueFeatureNamesfromList(listEA));
        	} else {
        		System.out.println("Available embedded annotations in " +workingDirectory +" for feature \"" +featureName +"\"");
        		for(EmbeddedAnnotation ea : listEA) {
        			if(ea.getFeature().equals(featureName)) {
        				System.out.print(ea);
        			}
        		}
        		System.out.println();
        	}
        	
        	return;
        }
                
//        System.out.println("feature = \"" +featureName +"\"");
//        System.out.println("message = " +commitMessage);
//        System.out.println("no-commit=" +commitNoCommit);
//        System.out.println("workingDirectory=" +workingDirectory);
		
		/**************************************************************/
		/** (1) SAVE CURRENT STATE OF WORKING DIRECTORY TO BACKUP DIR */
		/**************************************************************/
        String src = "\\src";
		File mainDir = new File(workingDirectory.concat(src));
		if (!mainDir.exists()) {
			System.out.println("ERROR: Working-Directory " +workingDirectory +" not found.");
			return;
		}
		File backUpDir = new File(mainDir + "_Backup");
		if (!backUpDir.exists()) {
			backUpDir.mkdir();
		} else {
			try {
				FileUtils.deleteDirectory(backUpDir);
				FileUtils.forceMkdir(backUpDir);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		try {
		    FileUtils.copyDirectory(mainDir,backUpDir);
		} catch (IOException e) {
		    e.printStackTrace();
		}
		
		Git git = null; 
//		git = gitInitRepository(git, mainDir);
		git = gitInitRepository(git, new File(workingDirectory));
		
		/*****************************************/
		/** (2) Check if staging area is empty and ask for deletion permit */
		/*****************************************/
		//System.out.println("Check staging area if empty");
		
		// TODO
//		if(1==1) {
//			return;
//		}
		
		
		
		/*****************************************/
		/** (3) Reset the Working Directory      */
		/*****************************************/
		try {
			git.reset().setMode(ResetCommand.ResetType.HARD).call();
			System.out.println("Git-Repository Reset success.");
		} catch (GitAPIException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		/*****************************************/
		/** (4) Get List of Embedded Annotations */
		/*****************************************/
		// Receive EA-List of project
		List<EmbeddedAnnotation> listEA = FAXE.extractEAfromRootDirectory(mainDir.getAbsolutePath());
		List<EmbeddedAnnotation> listEABackup = FAXE.extractEAfromRootDirectory(backUpDir.getAbsolutePath());
		
		
		/***************************************************************************************/
		/** Go through backup list an copy all changes of requested feature into Git-Directory */
		/***************************************************************************************/
		if(listEA.size()!=listEABackup.size()) {
			System.out.println("ERROR: UNKOWN SITUATION. listEA.size()!=listEABackup.size()");
			return;
		}
			
		System.out.println("Search for embedded annotation \"" +featureName +"\"");
		for(int i=0; i<listEA.size(); i++) {
			// check if searched feature
			String currentF = listEA.get(i).getFeature();
			if(currentF.equals(featureName)) {
				String backupF = listEABackup.get(i).getFeature(); 
				if(!backupF.equals(featureName)) {
					System.out.println("ERROR: UNKOWN SITUATION. !listEABackup.get(i).getFeature().equals(demoFeature)");
					return;
				}
				
				// -> Both EA Lists contain the same element. 
				// Check if content of parts is equal or different
				// Compare code in backup and working directory
				
				int openingLine = listEA.get(i).getOpeningLine();
				int closingLine = listEA.get(i).getClosingLine();
				boolean differenceFound = false;
				
				try {
					List<String> listLine = Files.readAllLines(Paths.get(listEA.get(i).getFile()));
					List<String> listLineBackup = Files.readAllLines(Paths.get(listEABackup.get(i).getFile()));
					for (int j = openingLine-1; j < (closingLine); j++) {	// openingLine-1 -> due to line count start with 1 and List-object with 0
						String line = listLine.get(j);
						String lineBackup = listLineBackup.get(j);
						
						if(!line.equals(lineBackup)) {
							differenceFound = true;
						} /*else {
							System.out.println(line);
							System.out.println("Lines identical");
						}*/
					}
				} catch (IOException e) {
					System.out.println(e);
				}

				
				// If different merge backup to working directory
				// If difference between source code parts found, copy full code from backup to working directory
				if(differenceFound==true) {
					// Delete EA content in working directory file
					System.out.println("Difference found in EA" +listEA.get(i).toString());
					
					try {
						List<String> listLine = Files.readAllLines(Paths.get(listEA.get(i).getFile()));
						for(int j=0; j<=(closingLine-openingLine); j++) {
							listLine.remove(openingLine-1);	// openingLine-1 -> due to line count start with 1 and List-object with 0
						}
						
						List<String> listLineBackup = Files.readAllLines(Paths.get(listEABackup.get(i).getFile()));
						
						
						int newOpeningLine = listEABackup.get(i).getOpeningLine();
						int newClosingLine = listEABackup.get(i).getClosingLine();
						for(int j=0; j<=(newClosingLine-newOpeningLine); j++) {
							String lineBackup = listLineBackup.get(newOpeningLine+j-1);	// openingLine-1 -> due to line count start with 1 and List-object with 0
							listLine.add(openingLine+j-1, lineBackup);	// openingLine-1 -> due to line count start with 1 and List-object with 0
						}
						
						//new File(listEA.get(i).getFile()).delete();
						Files.write(Paths.get(listEA.get(i).getFile()), listLine);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					/********************************/
					/** Add changed files to INDEX **/
					/********************************/
					try {
						// Define relative path from git root.
						String pattern = listEA.get(i).getFile();
						String tmp = pattern.substring(workingDirectory.length()+1).replace("\\", "/");
						git.add().addFilepattern(tmp).call();
//						git.add().addFilepattern(".").call();
						System.out.println("git-add performed.");
					} catch (GitAPIException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
				}	// if(differenceFound==true) {
								
			}	// currentF.equals(demoFeature)
			
		}	// for(int i=0; i<listEA.size(); i++)
		
		

		
		
		/******************************************************/
		/** Commit changes to repository (unless suppressed) **/
		/******************************************************/
	    if(!commitNoCommit) {
			if(commitMessage==null) {
				commitMessage = "Update Feature " +featureName +" in hole project (auto msg)";
			}

			try {
				git.commit()
//					    .setAuthor("...", "...@example.com") // Usage without author to use local setup
						.setMessage(commitMessage)
						.call();
			} catch (GitAPIException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("git-commit performed.");
		} else {
	    	System.out.println("NO git-commit performed.");
	    }
		
		
		/**********************************/
		/** CLEANUP                       */
		/**********************************/
		// Close Git
		git.close();
	    
		// Move data from backup to main
	    try {
	    	System.out.println("Cleaning Up ...");
	    	TimeUnit.SECONDS.sleep(2);
	    	FileUtils.deleteDirectory(mainDir);
	    	
	    	System.out.println("FileUtils.forceMkdir(mainDir)");
	    	TimeUnit.SECONDS.sleep(2);
			FileUtils.forceMkdir(mainDir);
			
			System.out.println("FileUtils.copyDirectory(backUpDir,mainDir)");
			TimeUnit.SECONDS.sleep(2);
		    FileUtils.copyDirectory(backUpDir,mainDir);
		    
		    // Delete backup folder
		    FileUtils.deleteDirectory(backUpDir);
			System.out.println("            ... Done");
		} catch (IOException e) {
		    e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
		
		System.out.println("<<< JGitPFC");
	}

	private static Git gitInitRepository(Git git, File mainDir) {
		/** ******************************************** */
		/** OPEN INSTANCE OF EXAMPLE PROJECT             */
		/** ******************************************** */
		try {
			git = Git.open(mainDir);
			System.out.println("Repository " +mainDir.getAbsolutePath() +" instantiated.");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("ERROR: no Git repository found");
			e.printStackTrace();
		}
		return git;
	}

}
